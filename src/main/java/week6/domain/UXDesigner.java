package week6.domain;

import java.util.Date;

public class UXDesigner extends Worker implements Designer {

    public UXDesigner(String name, String surname, Date date, String salary) {
        super(name, surname, date, salary);
    }

    @Override
    public void design() {
        System.out.println("My name is " +getName()+" "+getSurname()+".I work in Design Department as UX Designer.");
        System.out.println("(Salary: )"+getSalary());
    }

    @Override
    public void work() {
        design();
    }
}
