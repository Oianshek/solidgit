package week6.domain;

import java.util.Date;

public class ReactJSDeveloper extends Worker implements FrontEnd{

    public ReactJSDeveloper(String name, String surname, Date date, String salary) {
        super(name, surname, date, salary);
    }

    @Override
    public void writeFront() {
        System.out.println("I am"+getName()+" "+getSurname()+" and I'm front-end developer.(Salary: )"+getSalary());
    }

    @Override
    public void develop() {
        writeFront();
    }

    @Override
    public void work() {
        develop();
    }
}
