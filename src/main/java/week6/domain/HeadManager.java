package week6.domain;

import java.util.Date;

public class HeadManager extends Worker implements Manager {

    public HeadManager(String name, String surname, Date date, String salary) {
        super(name, surname, date, salary);
    }

    @Override
    public void manage() {
        System.out.println("I am"+getName()+" "+getSurname()+" and I'm President of our IT Company");
    }

    @Override
    public void work() {
        manage();
    }
}
