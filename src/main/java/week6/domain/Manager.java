package week6.domain;

public interface Manager extends Employee {
    void manage();
}
