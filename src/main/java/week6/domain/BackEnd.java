package week6.domain;

public interface BackEnd extends Developer{
    void writeBack();
}
