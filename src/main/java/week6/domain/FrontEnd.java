package week6.domain;

public interface FrontEnd extends Developer {
    void writeFront();
}
