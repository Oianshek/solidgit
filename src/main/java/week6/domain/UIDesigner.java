package week6.domain;

import java.util.Date;

public class UIDesigner extends Worker implements Designer{

    public UIDesigner(String name, String surname, Date date, String salary) {
        super(name, surname, date, salary);
    }

    @Override
    public void design() {
        System.out.println("I am "+getName()+" "+getSurname()+", I'm UI Designer.");
        System.out.println("(Salary: )"+getSalary());
    }

    @Override
    public void work() {
        design();
    }
}
