package week6.domain;

import java.util.Date;

public class Cleaner extends Worker implements Employee{

    public Cleaner(String name, String surname, Date date, String salary) {
        super(name, surname, date, salary);
    }

    @Override
    public void work() {
        System.out.println("I am "+getName()+" "+getSurname()+", I am cleaner in this company");
        System.out.println("(Salary: )"+getSalary());
    }
}
