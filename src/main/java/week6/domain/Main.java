package week6.domain;

import java.sql.Date;

public class Main {

    public static void main(String[] args) {
        /*Human h = new Human("Ayan","Sultan", Date.valueOf("2002-04-29"));
        System.out.println(h);*/

        Staff st = new Staff();
        st.addEmp(new HeadManager("Ayan","Sultan",Date.valueOf("2002-04-29"),"$150,000"));
        st.addEmp(new FinanceManager("Berik","Krutoi",Date.valueOf("2002-12-12"),"$45,000"));
        st.addEmp(new JavaDeveloper("Ulan","Buzhbanov",Date.valueOf("2002-05-07"),"$50,000"));
        st.addEmp(new ReactJSDeveloper("Jandos","Beisov",Date.valueOf("2002-12-12"),"$40,000"));
        st.addEmp(new UXDesigner("Esken","Musatai",Date.valueOf("2001-09-29"),"$40,000"));
        st.addEmp(new UIDesigner("Dauren","Ashim",Date.valueOf("2002-12-12"),"$40,000"));
        st.addEmp(new Cleaner("Someone","Someoneovich",Date.valueOf("1990-12-12"),"$5,000"));

        st.Work();
    }
}
