package week6.domain;

public interface Developer extends Employee{
    void develop();
}
