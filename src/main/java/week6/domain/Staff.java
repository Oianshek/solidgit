package week6.domain;

import java.util.ArrayList;

public class Staff {
    private ArrayList<Employee> emps = new ArrayList<Employee>();

    public void addEmp(Employee emp){
        emps.add(emp);
    }

    public void Work(){
        for (Employee employee : emps) employee.work();
    }
}
