package week6.domain;

import java.util.Date;

public class JavaDeveloper extends Worker implements BackEnd {

    public JavaDeveloper(String name, String surname, Date date, String salary) {
        super(name, surname, date, salary);
    }

    @Override
    public void writeBack() {
        System.out.println("I am"+getName()+" "+getSurname()+" and I'm back-end developer.");
        System.out.println("(Salary: )"+getSalary());
    }

    @Override
    public void develop() {
        writeBack();
    }

    @Override
    public void work() {
        develop();
    }
}
