package week6.domain;

import java.util.Date;

public class Human {
    private String name;
    private String surname;
    private Date date;

    public Human(String name, String surname, Date date){
        setSurname(surname);
        setName(name);
        setDate(date);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return name+" "+surname+" "+date;
    }
}
