package week6.domain;

import java.util.Date;

public class FinanceManager extends Worker implements Manager{

    public FinanceManager(String name, String surname, Date date, String salary) {
        super(name, surname, date, salary);
    }

    @Override
    public void manage() {
        System.out.println("My name is "+getName()+" "+getSurname()+" and I am Chief financier of our Company");
        System.out.println("(Salary: )"+getSalary());
    }

    @Override
    public void work() {
        manage();
    }
}
