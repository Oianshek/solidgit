package week6.domain;

public interface Designer extends Employee {
    void design();
}
