package week6.domain;

import java.util.Date;

public abstract class Worker extends Human implements Employee {
    private String salary;


    public Worker(String name, String surname, Date date, String salary) {
        super(name, surname, date);
        setSalary(salary);
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return super.toString()+" "+salary;
    }
}
